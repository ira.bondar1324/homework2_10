"use strict";

let list = document.querySelector(".tabs");
let arrTabs = list.children;
let listText = document.querySelector(".tabs-content");
let arrText = listText.children;

function removeClasses(){
    for (const elem of arrTabs){
        elem.classList.remove("active");
    }
}

function addingAtribut(){
    for(let i = 0;i<arrText.length;i++){
        arrText[i].setAttribute(`data-number`,i);
        arrText[i].style.display="none";
    }
}

function changeDisplay(){
    for(let i = 0;i<arrText.length;i++){
        arrText[i].style.display="none";
    }
}

addingAtribut();

for (let i=0;i<arrTabs.length;i++) {
        arrTabs[i].addEventListener("click",()=>{
            changeDisplay();
            removeClasses();
            arrTabs[i].classList.add("active");
            if(arrText[i].dataset.number === `${i}`){
                arrText[i].style.display="block";
            }
        }
    )
}  